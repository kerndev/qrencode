#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Windows.h>
#include "paint.h"
#include "qrencode.h"

void draw_bitmap(int line_size, int data_size, unsigned char *data)
{
	int i, j;
	for(i = 0; i < data_size; i++)
	{
		new_line();
		draw_block(0);
		for(j = 0; j < data_size; j++)
		{
			draw_block(data[j]);
		}
		data += line_size;
	}
	new_line();
}

unsigned char utf8_text[1024];
void UnicodeToUtf8(const wchar_t *unicode, unsigned char *utf8)
{
	int len;
	len = WideCharToMultiByte(CP_UTF8, 0, unicode, -1, NULL, 0, NULL, NULL);
	memset(utf8, 0, len + 1);
	WideCharToMultiByte(CP_UTF8, 0, unicode, -1, utf8, len, NULL, NULL);
}

int main(int argc, unsigned char **argv)
{
	int i;
	struct qrcode_bitmap bitmap;
	unsigned char text[256];
	int text_size[10]={17,32,53,78,106,134,154,192,230};
	memset(text, 0, 256);
	for(i=0; i<9; i++)
	{
		memset(text, i+0x31, text_size[i]);
		//strcpy(text, "123456");
		if(!qrcode_encode(text, strlen(text), &bitmap))
		{
			printf("ver: %d, failed!\n",i+1);
			break;
		}
		printf("size: %d * %d\n", bitmap.data_size, bitmap.data_size);
		begin_paint();
		draw_bitmap(bitmap.line_size, bitmap.data_size, bitmap.data);
		end_paint();
	}
	getchar();
	return 0;
}
